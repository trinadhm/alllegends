var GetLegends = {

	getAllLegends : function(conf){
		var config = {
			restStyleUrl: conf.restStyleUrl || "http://localhost:8080/geoserver/rest/styles.json",
			gservurl : conf.gservurl || "http://localhost:8080/geoserver/wms",
			user: conf.user || "admin",
			pass: conf.pass || "geoserver",
			layer: conf.layer || "usa:states",
			element : conf.element || "legends",
			size: conf.size || 60,
			labels : conf.labels || 'on'
		};


		var getLegends = function(){
			var table = document.createElement('table');
			tcontent = "<tr><th>Style</th><th>Name</th></tr>";
			var content = "<tr><td><img src='"+config.gservurl+"?REQUEST=GetLegendGraphic&VERSION=1.0.0&legend_options=forceLabels:"+config.labels+"&FORMAT=image/png&WIDTH="+config.size+"&HEIGHT="+config.size+"&transparent=true&LAYER="+config.layer+"&style=";
			var el=document.getElementById(config.element);
			var reqListener = function(){
				var stylesList = JSON.parse(this.responseText);
				stylesList = stylesList.styles.style;
				for (var x=stylesList.length-1; x>=0; x--){
					var temp = content+stylesList[x].name+"'></td><td> "+stylesList[x].name+"</td></tr>";
					tcontent += temp;
				}
				table.innerHTML = tcontent;
				el.appendChild(table);
			};
			var oReq = new XMLHttpRequest();
			oReq.onload = reqListener;
			oReq.open("get", config.restStyleUrl, true, config.user, config.pass);
			oReq.send();

		}();
	}
};
